﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(textID.Text);
            String nombre = textNOMBRE.Text;
            String apellido = textAPELLIDO.Text;
            int cedula = Convert.ToInt32(textCEDULA.Text);
            String tipocedula = textTIPOCEDULA.Text;


            Model modelo = new Model(id,nombre,apellido,cedula,tipocedula);
            modelo.insertarPersona();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
   
            int id = Convert.ToInt32(textID.Text);
            String nombre = textNOMBRE.Text;
            String apellido = textAPELLIDO.Text;
            int cedula = Convert.ToInt32(textCEDULA.Text);
            String tipocedula = textTIPOCEDULA.Text;


            Model modelo = new Model(id, nombre, apellido, cedula, tipocedula);
            modelo.actualizarPersona();
            
            
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(textID.Text);
      
            Model modelo = new Model(id);
            modelo.eliminarPersona();
        }
    }
}
