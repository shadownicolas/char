﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    class Model
    {
        private int id { get; set; }
        private string nombre { get; set; }
        private string apellido { get; set; }
        private int cedula { get; set; }
        private string tipocedula { get; set; }

        public Model(int id, string nombre, string apellido, int cedula, string tipocedula)
        {
            this.id = id;
            this.nombre = nombre;
            this.apellido = apellido;
            this.cedula = cedula;
            this.tipocedula = tipocedula;
        }
        public Model(int id)
        {
            this.id = id;
        }

        public bool insertarPersona()
        {
            bool resultado = true;

            string cadena = "server=localhost;user=root;database=crud;port=3306;password=12345;SSL Mode=none";
            MySqlConnection conexion = new MySqlConnection(cadena);
            conexion.Open();
            String sql = String.Format("insert into persona values('{0}','{1}','{2}','{3}','{4}')",this.id,this.nombre,this.apellido,this.cedula,this.tipocedula);
            MySqlCommand comando = new MySqlCommand(sql, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            return resultado;
        }

        public bool actualizarPersona()
        {
            bool resultado = true;
            string cadena = "server=localhost;user=root;database=crud;port=3306;password=12345;SSL Mode=none";
            MySqlConnection conexion = new MySqlConnection(cadena);
            conexion.Open();
            String sql = String.Format("UPDATE persona SET nombre='{1}',apellido='{2}',cedula='{3}',tipocedula='{4}' WHERE id='{0}'", this.id, this.nombre, this.apellido, this.cedula, this.tipocedula);
            MySqlCommand comando = new MySqlCommand(sql, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            return resultado;


        }
        public bool eliminarPersona()
        {
            bool resultado = true;
            string cadena = "server=localhost;user=root;database=crud;port=3306;password=12345;SSL Mode=none";
            MySqlConnection conexion = new MySqlConnection(cadena);
            conexion.Open();
            String sql = String.Format("DELETE FROM persona WHERE id='{0}'", this.id, this.nombre, this.apellido, this.cedula, this.tipocedula);
            MySqlCommand comando = new MySqlCommand(sql, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            return resultado;


        }


    }
}
